---
layout: page
title: About
current: about
cover: "assets/images/iOBTE2xsYko-unsplash.jpg"
unsplashid: "iOBTE2xsYko"
unsplashname: "@strong18philip"
short-cover: true
---

I swear these thoughts made sense when they passed through my head.

Built with ennui using {% include icon-github.html username="jekyll" %} /
[jekyll](https://github.com/jekyll/jekyll)
