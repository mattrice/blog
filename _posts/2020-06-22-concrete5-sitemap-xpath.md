---
title:  Manipulating a Concrete5 sitemap.xml with XPath
date:   2020-06-22 16:57:24
cover: 'assets/images/466ENaLuhLY-unsplash.jpg'
unsplashid: '466ENaLuhLY'
unsplashname: "@markusspiske"
tags: ['php', 'concrete5', 'xpath', 'xml', 'google search console', 'google']
subclass: 'post tag-content'
author: mattrice
categories: mattrice
---

An email from Google: "New Coverage Issue detected": just what I needed first thing on a Monday morning. /s

That issue was easily resolved (the crawler got a 500 error because of some scheduled downtime); however, while researching that alert I noticed a worrying entry: "Submitted URL blocked by robots.txt" with _hundreds of pages_ listed. Great.

## Start at the Beginning

A visual inspection of the URLs from Google shows that they all include `/shipyard/` in their path. I know that the convention has been to use a "hidden" portion of the website to build out new pages. I am sure we could have used something like unpublished pages to restrict pages under construction to authorized viewers only, but this is easier to get feedback from the average end-user. Keeping Google from indexing pages that are still works-in-progress seems reasonable.

The salient line from `robots.txt` is quoted below, and is even commented!

```
# Added 2016-04-29 to prevent Greg['s] temp sites from being indexed
Disallow: /shipyard
```

This codifies my recollection. Time to start investigating how Google is finding out about pages in The Shipyard.

## Alas poor sitemap.xml, I knew him!

Our current CMS, [Concrete5][concrete5], provides an "Automated Job" to periodically regenerate the Sitemap, and Google helpfully crawls the generated `sitemap.xml` file regularly. <a target="_blank" href="assets/files/2020-06-22_sitemap.xml">An abbreviated `sitemap.xml` is available</a> for your perusal. I also include a snippet below, for ease of reference:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
  <url>
    <loc>https://www.midmich.edu/</loc>
    <lastmod>2018-07-02T16:00:52-04:00</lastmod>
    <changefreq>weekly</changefreq>
    <priority>0.5</priority>
  </url>
  <url>
    <loc>https://www.midmich.edu/admissions</loc>
    <lastmod>2020-06-15T16:23:34-04:00</lastmod>
    <changefreq>weekly</changefreq>
    <priority>0.5</priority>
  </url>
  <url>
    <loc>https://www.midmich.edu/shipyard/test</loc>
    <lastmod>2019-11-19T14:56:56-05:00</lastmod>
    <changefreq>weekly</changefreq>
    <priority>0.5</priority>
  </url>
</urlset>
```

A fairly bog-standard sitemap. That explains where Google is learning about the pages in The Shipyard, so now I know where to focus my fix: the data being pulled.

## Taking a long walk off a short pier wearing Concrete5 Shoes

There is an easy way to prevent _one page_ from showing up in Concrete5's sitemap: [check one box in the Search Engine Optimization][c5-exclude-explained] and off you go.

However, preventing whole (sub-)trees is less feasible: [one forum post suggests a workaround][c5-recursive-workaround] that involves searchings and...setting each individual page's SEO setting. Furthermore, excluding every page has the nasty side-effect of requiring someone to remember to manually uncheck that box on every page that moves out of The Shipyard; feasible, but also error-prone particularly when moving a group of pages at once. I am certain there is a better solution.

## Now witness the firepower of this fully ARMED and OPERATIONAL XPath!

The most correct path here seems to be to modify the code to follow the human convention. [The source code for the `generate_sitemap` job][c5-sitemap-github] is not particularly involved, so dropping in a customization is fairly straightforward and should be relatively easy to merge when pulling in upstream updates (unless/until the job gets refactored ([which, of course, has already happened)][c5-sitemap-refactor-commit].

Since I do not have to deal with XML regularly, I always have to reeducate myself on XPath. Thankfully, long ago I bookmarked [an XPath Expression Testbed][xpath-testbed] and [an XPath cheatsheet][xpath-cheatsheet] that gets me up to speed quickly.

> This XPath stuff is great! I will _definitely_ remember this!  
~ Me, an optimist, every time I relearn XPath

### Namespace tripmines

It was easy enough to work out the XPath query I wanted:

```
//url[loc[starts-with(.,"https://www.midmich.edu/shipyard/")]]
# (find any url nodes with a child loc node with a text portion that starts with "https://www.midmich.edu/shipyard/")
```

However, because of the 'xmlns' attribute in the XML, PHP's DOMXPath object behaved differently than I would have expected: I would now have to prefix each node with a (user-defined) prefix if I wanted to get any matches; [I was reading this Namespace article from Microsoft][ms-namespaces] when I had my "Eureka!" moment.

It would certainly be easier for me, the user, if the DOMXPath object detected the single namespace and took care of all the complexity for me (and made it obvious what I would have to do in the case of multiple namespaces present), but this is the way.

For completeness, this is the final incarnation of the XPath query, ready for iteration (that probably will never come):

```php
// NOTE: no trailing slash on $url
$url = "https://www.midmich.edu/shipyard";
// $query uses the namespaced version of XPath
// '.' is essentially the same as 'text()', used for (unbenchmarked, but likely) performance improvements
$query = "//ns:url[ns:loc[.=\"$url\" or starts-with(.,\"$url/\")]]";

// Need to escape DOMXPath as the class has a declared namespace
$xpath = new \DOMXPath($dom);

// Register the XML namespace, assuming one exists
$ns = $dom->documentElement->namespaceURI;
$xpath->registerNamespace("ns", $ns);

$nodes = $xpath->query($query);

$count = $nodes->length;
// iterate last->first to remove all matched nodes
//  and avoid complaints from the DOMNodeList object
for($i = $count; --$i >= 0; ) {
    $node = $nodes[$i];
    $node->parentNode->removeChild($node);
}
```

## I love it when a plan comes together

Drop the new XPath find & remove logic into `generate_sitemap.php` and ~~call it a day~~ go work on something else.

## tl;dr

Stop providing contradictory information to Google, or suffer the (search console issue) consequences.

[concrete5]:            https://www.concrete5.org/
[c5-recursive-workaround]:  https://www.concrete5.org/community/forums/customizing_c5/why-is-exclude-from-sitemap.xml-not-recursive
[c5-exclude-explained]: https://www.concrete5.org/community/forums/customizing_c5/what-exactly-does-exclude-from-search-index-do
[c5-sitemap-github]:    https://github.com/concrete5/concrete5/blob/dbb5fe5229286be8b66a2c2d4efadada22595db4/concrete/jobs/generate_sitemap.php
[c5-sitemap-refactor-commit]:    https://github.com/concrete5/concrete5/commit/cea5b77c8147f2b957cce0d9ec18b93bbd00b088
[mattrice-cv]:          https://stackoverflow.com/cv/mattrice
[midmich]:              https://www.midmich.edu/
[ms-namespaces]:        https://docs.microsoft.com/en-us/archive/msdn-magazine/2001/july/the-xml-files-understanding-xml-namespaces#namespaces-in-xml
[xpath-cheatsheet]:     https://devhints.io/xpath
[xpath-testbed]:        http://www.whitebeam.org/library/guide/TechNotes/xpathtestbed.rhtm
