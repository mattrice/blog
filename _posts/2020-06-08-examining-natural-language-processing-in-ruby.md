---
title:  Evaluating Natural Language Processing options in Ruby
date:   2020-06-04 11:32:21
cover: 'assets/images/BsWgMBfb208-unsplash.jpg'
unsplashid: 'BsWgMBfb208'
unsplashname: "@heyerlein"
tags: research ruby gems API AI
subclass: 'post tag-content'
author: mattrice
categories: mattrice
---

An offhand comment sparked a thought: is there an easy, programmatic way to determine how positive or negative some user feedback is?

## Background

I was already working on our homegrown student opinion surveying tool (written in Ruby/Rails), and this seemed like a small, but impactful, feature I could squeeze in. So I set out to find out how easy\* it would be.

> We do these things not because they are easy, but because we thought they would be easy.  
>     ~Author Unknown (to me, at least)


## Initial Research

The first thing I had to do was find out how to map what I wanted to whatever would allow to Google for pre-existing solutions. I am not a linguist, so even arriving at the phrase "natural language processing" took longer than I want to admit. Further investigation of [a long, long list of NLP resources][nlp-list] led me to the specific class of problem I wanted to solve: [Sentiment Analysis][wikipedia-sentiment-analysis].

## Ready, Set, FIGHT

After not much more clicking, I found a couple of likely contenders: [TextMood][textmood] and [Sentimental][sentimental]. With these promising gems in hand I figured it was time for a little contest:

* Which gem could better interpret the written word?
* Would either match up with my interpretation?

Time to fire up the `rails console` and start prototyping a function:

``` ruby
irb> $sentiment = Sentimental.new(ngrams: 3)
irb> $sentiment.load_defaults
irb> $sentiment.threshold = 0.1
irb>
irb> $mood = TextMood.new(language: "en", normalize: true, start_ngram: 1, end_ngram: 3)
irb> $mood_type = TextMood.new(language: "en", normalize: true, start_ngram: 1, end_ngram: 3, ternary_output: true)
irb> $mood_types = { "-1" => :negative, "0" => :neutral, "1" => :positive }
irb>
irb> def check_sentiment(message: "")
irb>     puts "----TextMood--- #{$mood_types[$mood_type.analyze(message).to_s]} / #{$mood.analyze(message).round(2)}"
irb>     puts "--Sentimental-- #{$sentiment.sentiment(message)} / #{$sentiment.score(message).round(2)}"
irb> end
irb> check_sentiment(message: "terrible, horrible, no good, very bad day")
----TextMood--- negative / -1.0
--Sentimental-- negative / -1.13
=> nil
```

> Aside: yes, I use (icky) globals in my console sessions instead of taking the time to properly implement a Singleton (or three).
Quick and dirty techniques for a quick-and-dirty sandbox.

So far so good: I have a function that I can use to test a larger dataset. Although I would have expected a much more negative score, but this is my first foray, so maybe these are reasonable values? ¯\\_(ツ)_/¯

One more quick function to actually pull some data and we are ready to start focusing on the data instead of the method:

```ruby
irb> def run_nlp(ids: [])
irb> ids.each do |answer_id|
irb>     answer = Answer.find(answer_id)
irb>     puts "answer_id: #{answer_id}"
irb>     check_sentiment(message: answer.content)
irb>     end
irb> end
irb> run_nlp(ids: [813])
  Answer Load (0.7ms)  SELECT  `answers`.* FROM `answers` WHERE `answers`.`id` = 813 LIMIT 1
answer_id: 813
----TextMood--- positive / 5.3
--Sentimental-- positive / 5.98
=> [813]
```

It works!

## Test Results

Initial results were looking good: comments of "Best teacher", "terrible class", and "no comment" all spat out reasonable scores. And then I ran into this comment (abbreviated)

> This class was pure chaotic. The worst class of [school] so far. This class was VERY confusing the entire time. One resource said a due date, the other resource showed a different due date, it seemed like ALOT of homework, quizzes and tests for a TWO credit class. Lectures were also very confusing because the powerpoint that were posted on [the Learning Management System] and then the powerpoint that was gone over in class did not match up, it was very frustrating.

It was clear from the onset that this student was not happy, but TextMood and Sentiment disagreed: awarding a score of 12.75 and 13.54 respectively. Additionally, these were largest magnitude of score I had seen: not only were the libraries confident this comment was positive, but they were _very_ confident. Hmm.

## And Now For Something Completely Different

Clearly, neither of these two gems were going to do the job out-of-the-box. Part of my research into sentiment analysis tools pointed me towards IBM and Watson; I remember watching Watson on Jeopardy!, so maybe it can give better results than I was currently getting from my chosen gems.

In my testing, Watson's AI/ML had the best success rate (i.e. Watson's sentiment values matched how I interpreted an answer) with **21/26 ➯ ~81%** answers. There were another 4 answers that were reasonable disagreements, usually because the answer was short and contained implied information:

* e.g. `Answer.find(1086766)` ➯ `actually having lab when they pay for lab and have a lab class` - I rated this comment as negative while Watson rated it positive.

Watson only really tripped up on 1 answer:

* e.g. `Answer.find(1078066)`  ➯ `Honestly, I don't think I would change anything.` which seems positive, but Watson rated it negative.

TextMood and Sentimental also did well, but with more inconsistent results (**14/26 ➯ ~54%** and **17/26 ➯ ~65%** respectively). The large difference is that while both TextMood and Sentimental also suffered from disagreements I found reasonable, they also had multiple, glaringly bad classifications, even in so small a dataset.

For example, TextMood and Sentimental had trouble with a long answer (>2k characters):

* `Answer.find(1225309)` ➯ `...[Instructor] does not make a good learning or open environment for conversation...`

...rating both this comment and the "pure chaotic" from above as positive when they very, *very* clearly are not; Watson had no problems (correctly) classifying either answer as negative in nature.

## Subsequent Questions

Regardless of the sentiment analyzer used, there will clearly be instances where the automated, computed result and the human interpretation differ. This leads to a more insidious problem: if an automated system makes a bad judgement

- a) who fixes it?
- b) how is the fixer going to find out? and
- c) what kind of fix is possible?

One thing at a time:

The solutions to c) seem fairly easy to enumerate: i) fix it or ii) ignore it. Removing the bad value is not really an option since there would presumably be some background process that would see the comment needs to be processed and would dutifully (re-)process it, wasting time and effort just to recreate an already known bad state. "But we could ensure the process ignores this bad value!", you might say; how is that any different than ignoring the value from the start? Fixing the value raises new problems (designing a way to solicit the "correct" value, validation, saving, etc.), certainly possible, but not nearly as efficient as treating that record as if the analyzer could not reach a conclusion, which must be a reasonable end-state.

The most complete way to solve b) would be to have a human review every answer; however, we can toss this solution out as not only expensive (even student workers do not work for free), but duplicating work: why bother computing a score, if a human is going to do the same thing? Not even to mention the sensitivity of the information being reviewed: the faculty have historically been very paranoid about keeping survey results private.

We can actually avoid solving b) if we solve a) by having the faculty member fix it themselves! Not only do we avoid data sharing/confidentiality issues, but the issue will be discovered organically during review of the results, negating any staffing concerns. Additionally, the fixer will necessarily know about the existence of the issue, since they themselves determined it is a problem.

Therefore, a) the faculty member will fix issues themselves by c) telling the system to ignore bad results.

We have at least fleshed out the problem space: we need some way to assign a sentiment value and some way for faculty to ignore the assigned value.

## Expected Return on Effort

So, what kind of return could we expect? In the ideal case, instructors (and other people with access to the data, e.g. the deans) have one more piece of easily digestible information for every free-form answer. The app could also do things like sort comments from most positive comments to most negative (with un-analyzed/neutral/ignored comments sorted to the middle), etc.

One alternative to showing the score (and having to support users being able to force a generated score to be ignored) would be to only use the score behind the scenes to (e.g) sort comments. Doing so seems like such a minor gain it is hardly worth mentioning.

## **tl;dr**

In my opinion, the benefits to continue investigating/implementing this would not be worth the effort required. However, this could be a fairly low-effort way to incorporate Artificial Intelligence/Machine Learning.

[nlp-list]:     http://rubynlp.org/
[sentimental]:  https://github.com/7compass/sentimental
[textmood]:     https://github.com/stiang/textmood
[wikipedia-sentiment-analysis]: https://en.wikipedia.org/wiki/Sentiment_analysis
