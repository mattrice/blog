require 'liquid'
require 'uri'

module Jekyll
    # Adapted from a Stack Overflow answer
    # https://stackoverflow.com/questions/21315732/capitalize-first-letter-of-each-word-with-liquid-syntax/23453152#23453152
    module Camelcase
        def camelcase(words)
            return words.split(' ').map(&:capitalize).join(' ')
        end
    end
end

Liquid::Template.register_filter(Jekyll::Camelcase)