require 'liquid'
require 'uri'

module Jekyll
    # Adapted from a Stack Overflow answer
    # https://stackoverflow.com/questions/21315732/capitalize-first-letter-of-each-word-with-liquid-syntax/23453152#23453152
    module CountLimit
        # items is expected to be an array of strings
        # e.g. ["google sheets", "php", ["php","concrete5"]]
        # returns a flattened array, sorted by number of occurances (descending), limited to the number specified (10 by default)
        def countLimit(items = [], limit = 10)
            return Hash[items.flatten.tally.sort_by {|k,v| v}.reverse].collect {|k,v| k }.first(limit)
        end
    end
end

Liquid::Template.register_filter(Jekyll::CountLimit)